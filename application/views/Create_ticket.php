<div class="content">
    <div class="container-fluid">
        <form class="create-ticket-form animated fadeIn widget grid6" method="post" enctype="multipart/form-data">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-5 form-group">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3">Select Status</span>
                            <select name="priority" class="form-control input-inline">
                                <option value="">SELECT</option>
                                <option value="1">CRITICAL</option>
                                <option value="2">MAJOR</option>
                                <option value="3">MINOR</option>
                                <option value="4">TRIVIAL</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-1 form-group"></div>
                    <div class="col-sm-6 form-group">
                        <div class="input-group pull-right">
                            <button type="submit" class="btn btn-success btn-send"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> <span>SEND</span></button>
                        </div>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-sm-5 form-group">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3">Select Type Of Issue</span>
                            <select name="subject" class="form-control input-inline">
                                <option value="">Select Here</option>
                                <option value="1">DESIGN LAYOUT</option>
                                <option value="2">VIDEO  PRODUCTION</option>
                                <option value="3">WEBSITE LANDING PAGE</option>
                                <option value="4">WEBINAR/EVENTS</option>
                                <option value="5">EMAIL BLAST LAYOUT</option>
                                <option value="6">CRM SYSTEM/INFUSION</option>
                                <option value="7">CAMPAIGN MATERIAL</option>
                                <option value="8">SOCIAL MEDIA MATERIAL </option>
                                <option value="9">SEARCH ENGINE OPTIMIZATION</option>
                                <option value="10">CONTENT MATERIAL</option>
                                <option value="11">REPORT ANALYTICS</option>
                                <option value="12">UPLOADING/DOWNLOADING FILES</option>
                                <option value="13">OTHER TASK</option>
                            </select>
                        </div>
                    </div>
                     <div class="col-sm-2 form-group">
                         <input type="file" name="file[]" class="form-control fileupload" multiple="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword, application/vnd.ms-excel,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*">
                     </div>
                    <div class="col-sm-5 form-group">
                        <div class="input-group">
                            <input type="text" class="form-control fake-path" placeholder="doc,ppt,xsl,pdf,image only" readonly="readonly">
                            <span class="input-group-btn">
                                <button class="btn btn-default attached" type="button">Attach file <i class="fa fa-paperclip" aria-hidden="true"></i> MAX 5MB</button>
                            </span>
                        </div>
                    </div>
                  </div>
            </div>

            <div class="panel-body overflow-x">
                <div class="form-group"> 
                    <textarea class="create-ticket" name="create_ticket" id="create-ticket"></textarea>
                </div>
                <div class="progress hide">
                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                        0% Complete (success)
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="footer text-center muted">Ticketing System The Outsourced Accountant 2017</div>
</div>
<script>
    (function (j) {
        j(window).load(function () {

        });
    })(jQuery);
</script>  