<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View_transfer_report extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('cookie');
        $this->load->model('Models');
    }
    function index() {
        redirect(site_url());
    }
    function date(){
        $this->load->view('includes/header');
        if (($this->session->userdata('user_session'))) {
            $this->load->view('includes/header-nav');
            $this->load->view('includes/sidemenu');
            $this->load->view('View_transfer_report');
        } else {
            $this->load->view('Login');
        }
        $this->load->view('includes/footer');
    }
}
