<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Manila');
header('Content-Type: application/json');

class Response extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Models');
        $this->load->model('Modules');
        $this->load->database();
    }
    
    function getcsvinfo() {
        $target_dir = "uploads/csv.csv";
        $append = '[';
        $file = fopen($target_dir, "r");
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
            $id = $getData[0];
            $csvdata = array(
                "email" => strtolower($getData[0]),
                "password" => md5(sha1('123123')),
                "position" => "",
                "fname" => $getData[2],
                "lastname" => $getData[3],
                "job_title" => $getData[4],
                "location" => $getData[5]
            );
            $datavalidation = array("email" => $getData[0]);
            $validate_exist = $this->Modules->datavalidation($datavalidation);
            //if ($getData[0] != "Email" && ) { // validate email
            if ($getData[0] != "Email" && $validate_exist == 'success' && strlen($getData[2]) != 0) { // validate email
                if (filter_var($getData[0], FILTER_VALIDATE_EMAIL)) {
                    $insertdata = $this->Modules->fromcsvuser($csvdata);
                    $append .= '{"message":"success","text":"' . $validate_exist . '","type":"success", "email":"'.$getData[0].'","fname":"'.$getData[2].'","lastname":"'.$getData[3].'","position":"'."".'","job_title":"'.$getData[4].'","location":"'.$getData[5].'"},';
                }
            }
        }
        $append = rtrim($append, ',');
        $append .= ']';
        print $append;
    }

    function csvimport() {
        $target_dir = "uploads/csv.csv";
        
        $filename = $_FILES["fileToUpload"]["tmp_name"];
        $extension = pathinfo($_FILES["fileToUpload"]["name"], PATHINFO_EXTENSION);
        $upload = move_uploaded_file($filename, $target_dir);
        print json_encode(array("message"=>"success", "validation"=>$extension));
    }

    function index() {
        redirect(site_url());
    }
    function exit_status($str) {
        echo json_encode(array('status' => $str));
        exit;
    }

    function get_extension($file_name) {
        $ext = explode('.', $file_name);
        $ext = array_pop($ext);
        return strtolower($ext);
    }

    function add_users() {
        $this->form_validation->set_rules('firstname', 'firstname', 'required');
        $this->form_validation->set_rules('lastname', 'lastname', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('user_type', 'user_type', 'required');
        $this->form_validation->set_rules('location', 'location', 'required');
        $this->form_validation->set_rules('JobTitle', 'JobTitle', 'required');
        if ($this->form_validation->run()) {
            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $middlename = $this->input->post('middlename');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $position = $this->input->post('user_type');
            $user_type = $this->input->post('user_type');
            $location = $this->input->post('location');
            $JobTitle = $this->input->post('JobTitle');
           
            if($user_type == 3){
                $value_usertype = "";
            }else{
                $value_usertype = $user_type;
            }
            
            $data = array(
                'email' => $email,
                'password' => md5(sha1($password)),
                'fname' => $firstname,
                'middle' => $middlename,
                'lastname' => $lastname,
                'position' => $value_usertype,
                'location' => $location,
                'job_title' => $JobTitle,
                'online' => 0,
                'last_login' => 0
            );
            $execute_data = $this->Modules->add_user_info($data);
            if ($execute_data == 'exist') {
                $value = array(
                    "message" => "success",
                    "text" => "Existing Account!",
                    "type" => "info"
                );
            } else {
                $value = array(
                    "message" => "success",
                    "text" => "Save",
                    "type" => "info"
                );
            }
        } else {
            $value = array(
                "message" => "error",
                "text" => "Error!",
                "type" => "danger"
            );
        }

        print json_encode($value);
    }
/*
    function reset_password() {
        $idxx = $this->input->post('password_id_reset_all');
       // $password = md5(sha1("xxx12345"));
        $password =md5(sha1($idxx));
        $data = array( 'password' => $password);
        $this->db->where('ID', $idxx);
        $this->db->update('user_info', $data);
        $value = array(
            "message" => "success",
            "text" => 'Updated Succesfully',
            "type" => "info"
        );
        print json_encode($value);
    }
 * /
 */
    
   function reset_password() {
        $id = $this->input->post('password_id');
        $repassword = $this->input->post('repassword');
       // $this->Module->reset_user_password($id, $repassword);
        $data = array( 'password' => md5(sha1($repassword)));
        $this->db->where('ID', $id);
        $this->db->update('user_info', $data);
        
        $value = array(
            "message" => "success",
            "text" => 'Updated Succesfully (' . $repassword . ")",
            "type" => "info"
        );
        print json_encode($value);
    }  
    
    
    

    function update_user() {
        $id = $this->input->post('user_value');
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $middlename = $this->input->post('middlename');
        $email = $this->input->post('email');
        $password = md5(sha1($this->input->post('password')));
        $position = $this->input->post('position');
        $job_title_update = $this->input->post('job_title_update');
        $location_update = $this->input->post('location_update');
        
        $data = array(
            'fname' => $firstname,
            'middle' => $middlename,
            'lastname' => $lastname,
            'email' => $email,
            'position' => $position,
            'job_title' => $job_title_update,
            'location' => $location_update
            
        );
        $this->db->where('ID', $id);
        $this->db->update('user_info', $data);
        $value = array(
            "message" => "success",
            "text" => 'Updated Succesfully (' . $firstname . ")",
            "type" => "info"
        );
        print json_encode($value);
    }

    function delete_user_r() {
        $id = $this->input->post('password_id');
        $repassword = $this->input->post('repassword');
        $this->Modules->delete_user($id);

        $value = array(
            "message" => "success",
            "text" => 'Deleted Succesfully (' . $id . ")",
            "type" => "info"
        );
        print json_encode($value);
    }

    function create_ticket_guest(){
        
        $this->form_validation->set_rules('priority', 'priority', 'required');
        $this->form_validation->set_rules('subject', 'subject', 'required');
        $this->form_validation->set_rules('create_ticket', 'create_ticket', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('firstname', 'firstname', 'required');
        $this->form_validation->set_rules('lastname', 'lastname', 'required');
        $this->form_validation->set_rules('location', 'location', 'required');
       
        if ($this->form_validation->run()) {
             
            $key_id = strtotime("NOW") . rand(10, 99);
           
            $insert_user_info = array(
                'email' => $this->input->post('email'),
                'job_title' => 'Guest Ticket',
                'password' => md5(sha1('123123')),
                'position' => '',
                'fname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'location' => $this->input->post('location')
            );
           $validate_if_exist =  $this->Modules->add_info($insert_user_info);
           
           $execute_info_details= $this->Modules->select_users($this->input->post('email'));
             $insert = array(
                'priority' => $this->input->post('priority'),
                'subject' => $this->input->post('subject'),
                'message' => $this->input->post('create_ticket'),
                'key_id' => $key_id,
                'sender' => $execute_info_details,
                'date' => strtotime("NOW"),
                'status' => '1'
            );
          
              
            
            if (isset($_FILES['file'])) {
                $total = count($_FILES['file']['name']);
                $ext = array("doc", "docx", "ppt", "pptx", "xls", "xlsx", "pdf", "jpg", "jpeg", "JPG", "png", "gif");
                for ($i = 0; $i < $total; $i++) {
                    $fname = $_FILES['file']['name'][$i];
                    if (in_array(pathinfo($fname, PATHINFO_EXTENSION), $ext)) {
                        $tmpFilePath = $_FILES['file']['tmp_name'][$i];
                        move_uploaded_file($tmpFilePath, 'uploads/' . $key_id . '_' . $fname);
                        $array = array(
                            'key_id' => $key_id,
                            'filename' => $key_id . '_' . $_FILES['file']['name'][$i],
                        );
                        $this->Models->new_file($array);
                    }
                }
            }
           
           // $message_id = $this->Models->latest_ticket('Guest');
            //$_id = $message_id->result()[0]->ID;
            $this->Models->create_new_ticket($insert);
           $value = array("message" => "success", "text" => "Success", "message_id" => '2');
        }else{
            $value = array("message" => "error","text" => "Status,Type Of Issue and Description are requried");
        }
      
        
        
      // $value = array("message" => "success", "text" => "Success", "message_id" => '2');
        print json_encode($value);
    }
}
