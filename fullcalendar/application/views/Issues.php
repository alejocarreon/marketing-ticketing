<?php
if (is_numeric($this->uri->segment(3))) {
    $this->uri->segment(3);
    $_data = $this->Models->ticket_issue_id($this->uri->segment(3));
    if ($_data->num_rows() === 0) {
        redirect(site_url());
    }
    $_data = $_data->result();
    $_assn = $this->Models->ticket_assignee($this->uri->segment(3));
    $user_position = $this->session->userdata('user_position');
} else {
    redirect(site_url());
}
?>
<div class="content">
   <div class="container-fluid">
      <div class="animated fadeIn widget grid6">
         <div class="panel-heading">
            <div class="row">
               <div class="col-sm-6 form-group">
                  <form class="input-group message-status form-group-sm">
                     <span class="input-group-addon">
                        <span>STATUS</span>
                        <input type="hidden" value="<?php echo $this->uri->segment(3) ?>" class="hide message-status-input" name="message_status">
                     </span>

                     <input type="hidden" value="<?php echo $_data[0]->email ?>" name="sender_update" >
                     <select name="status" class="form-control input-inline"  data-select="<?php echo $_data[0]->status ?>">
                        <option value="4">RESOLVED</option>
                        <option value="3">ONGOING</option>
                        <option value="2">RE OPEN</option>
                        <option value="1">OPEN</option>
                     </select>
                     <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm" type="submit" ><i class="fa fa-pencil" aria-hidden="true"></i> UPDATE STATUS</button>
                     </span>


                  </form>
               </div>
               <div class="col-sm-6 form-group">
                   <?php if ($user_position !== '') { ?>
                      <div class="input-group  form-group-sm">
                         <span class="input-group-addon"><span>ADD ASSIGNEE</span></span>
                         <input type="text" class="form-control input-inline find-assignee" data-bind="<?php echo $this->uri->segment(3) ?>">
                      </div>
                  <?php } ?>
               </div>
            </div>
            <div class="row">
               <div  class="col-sm-6 form-group">
                  <div class="input-group  form-group-sm assignee-form" data-bind="<?php echo $this->uri->segment(3) ?>">
                     <span class="input-group-addon"><span>ASSIGNEE</span></span> 
                     <div class="form-control" style="overflow: hidden">
                         <?php foreach ($_assn->result() as $_it) { ?>
                            <strong class="label label-info">
                                <?php echo $_it->fname ?> <?php echo $_it->lastname ?> 
                                <?php if ($user_position !== '') { ?>
                                   <a href="<?php echo $_it->GETID ?>" class="fa fa-times text-danger"></a>
                               <?php } ?>
                            </strong> 
                        <?php } ?>
                     </div> 
                  </div>              
               </div>
               <div class="col-sm-3 form-group">
                  <div class="input-group  form-group-sm">
                     <span class="input-group-addon"><span>SUBJECT</span></span> 
                     <?php if ($user_position === '') { ?>
                         <div class="form-control"><?php echo $this->Models->issue_type($_data[0]->subject) ?></div>
                     <?php } else { ?>
                         <select class="update-subject form-control input-xs input-inline" data-bind="<?php echo $this->uri->segment(3) ?>" data-select="<?php echo $_data[0]->subject ?>">
                            <option value="">SELECT HERE</option>
                            <option value="1">TRANSFER/SETUP COMPUTER</option>
                            <option value="2">INTERNET ISSUES</option>
                            <option value="3">NETWORK ISSUES</option>
                            <option value="4">HARDWARE ISSUES/CONCERN</option>
                            <option value="5">SOFTWARE ISSUES/CONCERN</option>
                            <option value="6">MARKETING</option>
                            <option value="7">SYSTEM</option>
                         </select>
                     <?php } ?>
                  </div>
               </div>
               <div  class="col-sm-3 form-group">
                  <div class="input-group  form-group-sm">
                     <span class="input-group-addon"><span>PRIORITY</span></span> 
                     <div class="form-control text-center"><?php echo $this->Models->priority($_data[0]->priority); ?></div> 
                  </div>              
               </div>
            </div>  
            <div class="row">
               <div  class="col-sm-6 form-group">
                  <div class="input-group form-group-sm">
                     <span class="input-group-addon"><span>DATE</span></span> 
                     <?php if ($user_position === '') { ?>
                         <div class="form-control"><?php echo date('m/d/Y h:i:s A', $_data[0]->date) ?></div> 
                     <?php } else { ?>
                         <input type="text" class="form-control update-issue-date" data-bind="<?php echo $this->uri->segment(3) ?>" value="<?php echo date('m/d/Y h:i:s A', $_data[0]->date) ?>" />
                     <?php } ?>
                  </div>              
               </div>
               <div  class="col-sm-6 form-group">
                  <div class="input-group form-group-sm">
                     <span class="input-group-addon"><span>SENDER</span></span> 
                     <div class="form-control"><?php echo $_data[0]->fname ?> <?php echo $_data[0]->lastname ?></div> 
                  </div>
               </div>
               <div  class="col-sm-12 form-group">
                  <div class="input-group form-group-sm cc-user" data-bind="<?php echo $this->uri->segment(3) ?>">
                     <span class="input-group-addon cc-name-label">
                        <i class="fa fa-users"></i>
                     </span> 
                     <input type="text" class="form-control cc-input-search" data-bind="<?php echo $this->uri->segment(3) ?>"  placeholder="CC Search by name or email" value="">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                   <?php $attached = $this->Models->ticket_attached($_data[0]->key_id); ?>
                   <?php if ($attached->num_rows() > 0) { ?>
                      <div class="well well-sm">
                          <?php foreach ($attached->result() as $_file) { ?>
                             <a href="<?php echo site_url('request/download?file=' . $_file->filename) ?>" target="blank" class="btn btn-default btn-xs"><?php echo $_file->filename ?></a>
                         <?php } ?>
                      </div>
                  <?php } ?>
               </div>
            </div>
         </div>
         <div class="panel-body overflow-x">

            <h4><i class="fa fa-bug" aria-hidden="true"></i> DESCRIPTION</h4>
            <hr>
            <div class="description">
               <div class="description-container">
                   <?php echo $_data[0]->message ?>
               </div>
               <form class="edit-textarea hide">
                  <div class="form-group">
                     <input type="hidden" class="hide" value="<?php echo $this->uri->segment(3); ?>" name="edit_descptn_id" />
                     <textarea class="edit-descptn" id="edit-descptn" name="edit_descptn"><?php echo $_data[0]->message ?></textarea>
                  </div>
                  <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-comment" aria-hidden="true"></i> UPDATE</button>
               </form>
               <div class="text-right show-edit-textarea">
                  <button class="btn-primary btn"><i class="fa fa-pencil"></i> Edit Description</button>
               </div>
            </div>
            
            
            <hr>
            <h4><i class="fa fa-star-half-o" aria-hidden="true"></i> RATE US</h4>
            <form class="stars">
               <?php 
               $query = $this->Models->check_ratings_issues($this->uri->segment(3));
               $r = $query->result();
               ?>
               <div class="star1 start-rating<?php  echo (isset($r[0])?($r[0]->rate_number > 0?' star-checked':''):''); ?>">
                  <input type="radio" class="star-radio" value="1" name="rating" title="1 Star" data-toggle="tooltip" data-placement="top"/>
                  <div class="star2 start-rating<?php  echo (isset($r[0])?($r[0]->rate_number > 1?' star-checked':''):''); ?>">
                     <input type="radio" class="star-radio" value="2" name="rating" title="2 Stars" data-toggle="tooltip" data-placement="top"/>
                     <div class="star3 start-rating<?php  echo (isset($r[0])?($r[0]->rate_number > 2?' star-checked':''):''); ?>">
                        <input type="radio" class="star-radio" value="3" name="rating" title="3 Stars" data-toggle="tooltip" data-placement="top"/>
                        <div class="star4 start-rating<?php echo (isset($r[0])?($r[0]->rate_number > 3?' star-checked':''):''); ?>">
                           <input type="radio" class="star-radio" value="4" name="rating" title="4 Stars" data-toggle="tooltip" data-placement="top"/>
                           <div class="star5 start-rating<?php echo (isset($r[0])?($r[0]->rate_number > 4?' star-checked':''):''); ?>">
                              <input type="radio" class="star-radio" value="5" name="rating" title="5 Stars" data-toggle="tooltip" data-placement="top"/>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
            <hr>
            <h4><i class="fa fa-comment" aria-hidden="true"></i> COMMENTS</h4>
            <hr>   
            <div class="comment-output" data-bind="<?php echo $this->uri->segment(3) ?>" data-id="<?php echo $this->session->userdata('user_session') ?>">
               <div class="list-group">
                   <?php
                   $comment = $this->Models->id_comment($this->uri->segment(3));
                   foreach ($comment->result() as $_comments) {
                       ?>
                      <div class="list-group-item">
                         <div class="comment-item-list">
                            <img class="add-profile" src="<?php echo site_url('request/image_id/' . $_comments->UID) ?>.jpg">
                         </div>
                         <div class="comment-item-pro">
                             <?php if ($_comments->UID === $this->session->userdata('user_session')) { ?>
                                <a class="close text-danger" href="<?php echo $_comments->ID; ?>"><i class="fa fa-times"></i></a>
                            <?php } ?>
                            <p><strong<?php ($_comments->UID === $this->session->userdata('user_session') ? ' class="text-info"' : '') ?>><?php echo $_comments->fname; ?> <?php echo $_comments->lastname; ?></strong></p>
                            <?php echo $_comments->comments; ?>
                         </div>
                      </div>
                  <?php } ?>
               </div>
            </div>
            <form class="comment-form">
               <input type="hidden" value="<?php echo $this->session->userdata('user_session'); ?>" name="user_id">
               <input type="hidden" value="<?php echo $this->uri->segment(3); ?>" name="comment_id">
               <div class="form-group">
                  <textarea class="comment-editor" id="comment_editor" name="comment"></textarea>
               </div>
               <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-comment" aria-hidden="true"></i> COMMENT</button>
            </form>
         </div>
         <div class="panel-footer text-right">
         </div>
      </div>
   </div>
   <div class="footer text-center muted">Ticketing System The Outsourced Accountant 2017</div>
</div>


  <div class="modal fade modal-rating" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Feed back</h4>
        </div>
         <div class="modal-body">
            <form class="form-suggestion">
               <p>(You clicked "<span class="rating-numb"></span>")</p>
               <input type="hidden" value="" name="sender_rate"  class="rating-numb-val">
               <input type="hidden" value="<?php echo $this->session->userdata('user_session') ?>" name="sender_id">
               <input type="hidden" value="<?php echo $this->uri->segment(3) ?>" name="issue_id">
               <strong>Comment/Suggestion</strong>
               <div class="form-group">
                <textarea class="form-control" name="suggestion"></textarea>
               </div>
            </form>
            
            <div class="success-rating hide">
               <span class="feedback-message">We're delighted that you are so happy with us at the moment and we really appreciate your feedback.</span>
               <br><br>
               Thanks
               <br><br>    
               IT Team 
            </div>
         </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-default hide ok-rating" data-dismiss="modal">Ok</button>
          <button type="button" class="btn btn-primary submit-suggestion">Submit</button>
        </div>
      </div>
    </div>
  </div>