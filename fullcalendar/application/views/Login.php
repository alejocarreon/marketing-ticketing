
<form class="login" method="post" autocomplete="false">
    <div class="text-center">
        <img alt="toa" src="<?php echo site_url('images/logo.png'); ?>">
    </div>
    <div class="form-group">
        <label class="input-group input-add-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" class="form-control" name="email_address" value="" placeholder="Email">
        </label>
    </div>
    <div class="form-group">
        <label class="input-group input-add-group">
            <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
            <input type="password" class="form-control" name="user_password" value="" placeholder="Password">
        </label>
    </div>
    <div class="checkbox">
        <div class="custom-input remember_me">
            <input type="checkbox" name="remember_me" value="1" id="remember"><label for="remember">Remember me</label>
        </div> 
    </div>
    <div class="form-group">
        <button class="btn btn-primary btn-block"><i class="fa fa-sign-in" aria-hidden="true"></i> LOGIN</button>
    </div>
         <div class="form-group">
        <a href="<?php echo base_url('guest'); ?>" class="btn btn-primary btn-block"><i class="fa fa-user" aria-hidden="true"></i> GUEST</a>
    </div>
    <div class="output"></div>
</form>
<div class="login-footer text-center muted">Ticketing The Outsourced Accountant 2017</div>