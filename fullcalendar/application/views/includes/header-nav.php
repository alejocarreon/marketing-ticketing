<nav class="navbar navbar-default navbar-fixed-top">
   <div class="container-fluid">
      <img src="<?php echo site_url('images/TOA logo.png') ?>" class="nav-logo">
      <ul class="nav navbar-nav navbar-right">
         <li id="fat-menu" class="dropdown"> 
            <a href="#" class="user-cache dropdown-toggle" id="drop2" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
               <i class="fa fa-ticket" aria-hidden="true"></i> Check Your Ticket Status
            </a>
            <ul class="list-group dropdown-menu it_status_menu"  aria-labelledby="drop2" data-bind="<?php echo $this->session->userdata('user_session') ?>">
            </ul>
         </li>
         <li id="fat-menu" class="dropdown">
             <?php
             $query_data = $this->Models->user_id($this->session->userdata('user_session'));
             $name_data = $query_data->result();
             ?>
            <a href="#" class="user-cache dropdown-toggle" id="drop3" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
               <i class="fa fa-user " aria-hidden="true"></i>
               <?php echo $name_data[0]->fname . ' ' . $name_data[0]->lastname; ?>
               <span class='caret'></span>
            </a>
            <ul class=dropdown-menu aria-labelledby="drop3">
               <li>
                  <a href='<?php echo site_url('settings') ?>' class="text-right">
                     SETTINGS <i class="fa fa-cog" aria-hidden="true"></i>
                  </a>
               </li>
               <li role=separator class=divider></li>
               <li><a href="#" class="logout-btn text-right">LOGOUT <i class="fa fa-sign-out " aria-hidden="true"></i></a></li>
            </ul>
         </li>
      </ul>
   </div>
</nav>